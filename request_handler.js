/* nodejs/test/request_handler.js */

// requires - 'npm config set strict-ssl false' then install fs.extra - 'sudo npm install --save fs.extra'
var fs = require("fs");
const net_request = require('request');

function getData(params, request, response)
{
	console.log("getData : " + params.action);
	console.log("symbols : " + params.symbols);
	if(params.action != null)
	{
		// Yahoo			//var request = "https://uk.finance.yahoo.com/quote/" + currentStockId + "?p=" + currentStockId;
		// Yahoo			Example: "https://query2.finance.yahoo.com/v7/finance/quote?symbols=BHP.ax"
									//		symbols = SIA.L,TLW.L
									//var request = "https://query2.finance.yahoo.com/v7/finance/quote?symbols=" + symbols
									//var request = "https://query2.finance.yahoo.com/v7/finance/quote?symbols=PMO.L,SIA.L,"

// https://query2.finance.yahoo.com/v10/finance/quoteSummary/AAPL?formatted=true&lang=en-US&region=US&modules=defaultKeyStatistics%2CfinancialData%2CcalendarEvents&corsDomain=finance.yahoo.com
//exports.HISTORICAL_CRUMB_URL = 'https://finance.yahoo.com/quote/$SYMBOL/history';
//exports.HISTORICAL_DOWNLOAD_URL = 'https://query1.finance.yahoo.com/v7/finance/download/$SYMBOL';
//exports.SNAPSHOT_URL = 'https://query2.finance.yahoo.com/v10/finance/quoteSummary/$SYMBOL';

		// Bloomberg	Example: "https://www.bloomberg.com/markets/api/bulk-time-series/price/SIA%3ALN?timeFrame=1_DAY";
		//						Example: "https://www.bloomberg.com/markets/api/bulk-time-series/price/APPL%3AUS?timeFrame=1_DAY";
									//		symbols = SIA.LN
									//var stock = symbols.substring(0,3);
									//var market = symbols.substring(4);
									//var request = "https://www.bloomberg.com/markets/api/bulk-time-series/price/" + stock + "%3A" + market + "?timeFrame=1_DAY";

		if(params.action == "stocks") {
			var req = "https://query2.finance.yahoo.com/v7/finance/quote?symbols=" + params.symbols;
			console.log("Sending request: " + req);
			net_request(req, function(err, res, data)
			{
				response.writeHead(200);
				response.write(data);
				response.end();
			});
		}
		//London Stock Exchange
		//https://www.londonstockexchange.com/exchange/news/market-news/market-news-home.html?nameCodeText=SIA&searchType=searchForNameCode&nameCode=SIA&text=&rnsSubmitButton=Search&activatedFilters=true&newsSource=ALL&mostRead=&headlineCode=ONLY_EARNINGS_NEWS&headlineId=&ftseIndex=&sectorCode=&rbDate=released&preDate=LastMonth&newsPerPage=20
		else if(params.action == "rns") {
			var stock = params.symbols;
			stock = stock.substring(0,3);
			var req = "https://www.londonstockexchange.com/exchange/news/market-news/market-news-home.html?nameCodeText=" + stock + "&searchType=searchForNameCode&nameCode=" + stock + "&text=&rnsSubmitButton=Search&activatedFilters=true&newsSource=ALL&mostRead=&headlineCode=ONLY_EARNINGS_NEWS&headlineId=&ftseIndex=&sectorCode=&rbDate=released&preDate=LastMonth&newsPerPage=20";
			//var req = "https://www.londonstockexchange.com";
			console.log("Sending request: " + req);
			net_request(req, {strictSSL: false}, function(err, res, html)
			{
				console.log("ERROR: " + err);
				console.log("RETURN: \n" + html);
				response.writeHead(200);
				response.write(html);
				response.end();
			});
		}
	}
}

function filecss(pathname, response)
{
	console.log("Request handler ." + pathname + " was called.");
	fs.readFile("." + pathname, function(err, data)
	{
		if(err)
		{
			console.log(err);
			console.log("No request for: " + pathname);
			response.writeHead(404);	// HTTP request Not Found
			response.write("404 File Not Found");
			response.end();
		}
		else
		{
			console.log("Serving Request for ." + pathname);
			response.writeHead(200, {'Content-Type': 'text/css'});	// HTTP request Successful
			response.write(data);
			response.end();
		}
	});
}

function filejpg(pathname, response)
{
	console.log("Request handler ." + pathname + " was called.");
	fs.readFile("." + pathname, function(err, data)
	{
		if(err)
		{
			console.log(err);
			console.log("No request for: " + pathname);
			response.writeHead(404);	// HTTP request Not Found
			response.write("404 File Not Found");
			response.end();
		}
		else
		{
			console.log("Serving Request for ." + pathname);
			response.writeHead(200);	// HTTP request Successful
			response.write(data);
			response.end();
		}
	});
}

function filepng(pathname, response)
{
	console.log("Request handler ." + pathname + " was called.");
	fs.readFile("." + pathname, function(err, data)
	{
		if(err)
		{
			console.log(err);
			console.log("No request for: " + pathname);
			response.writeHead(404);	// HTTP request Not Found
			response.write("404 File Not Found");
			response.end();
		}
		else
		{
			console.log("Serving Request for ." + pathname);
			response.writeHead(200);	// HTTP request Successful
			response.write(data);
			response.end();
		}
	});
}

function filejs(pathname, response)
{
	console.log("Request handler ." + pathname + " was called.");
	fs.readFile("." + pathname, function(err, data)
	{
		if(err)
		{
			console.log(err);
			console.log("No request for: " + pathname);
			response.writeHead(404);	// HTTP request Not Found
			response.write("404 File Not Found");
			response.end();
		}
		else
		{
			console.log("Serving Request for ." + pathname);
			response.writeHead(200, {'Content-Type': 'text/javascript'});	// HTTP request Successful
			response.write(data);
			response.end();
		}
	});
}

function index(pathname, response)
{
	console.log("Request handler 'index' was called.");
	console.log("Current Working Dir: " + process.cwd());
	fs.readFile("./stocks/index/index.html", function(err, data)
	{
		if(err)
		{
			console.log(err);
			console.log("No request for: ./stocks/index.html");
			response.writeHead(404);	// HTTP request Not Found
			response.write("404 File Not Found");
			response.end();
		}
		else
		{
			console.log("Serving Request for ./stocks/index.html");
			response.writeHead(200);
			response.write(data);
			response.end();
		}
	});
}

exports.filecss = filecss;
exports.filejpg = filejpg;
exports.filejs = filejs;
exports.filepng = filepng;
exports.getData = getData;
exports.index = index;
