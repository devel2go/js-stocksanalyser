up: deploy server

down: kill-server

server:
	cd /home/richard/stockserver
	forever start index.js

kill-server:
	cd /home/richard/stockserver
	forever stop index.js

deploy:
