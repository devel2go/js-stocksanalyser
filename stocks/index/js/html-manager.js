function stockIndex(stockId, watchList, country, language)
{
  var lg = language;
	var html = "";
	var html1 = "";
  var html2 = "";
  var market = "";
  var symbol = "";
  var stockExchange = "";

  if(country == "UK") stockExchange = "LSE";
  html1 = html1 + "<li id=\"SECTION001\">" + stockExchange + "<ul>";
  for(var ckey in watchList) {
    if(watchList.hasOwnProperty(ckey)) {
      if(ckey == stockExchange) {
        for(var mkey in watchList[ckey]) {
          if(watchList[ckey].hasOwnProperty(mkey)) {
            market = mkey;
						html1 = html1 + "<li class=\"LinkedList\" id=\"\">" + market + "<ul>";
            for(var skey in watchList[ckey][mkey]) {
              if(watchList[ckey][mkey].hasOwnProperty(skey)) {
                symbol = skey;
                //for(var i in watchList[ckey][mkey]) {
                  //symbol = watchList[ckey][mkey][i];
                html1 = html1 + "<li class=\"LinkedList\" id=\"" + symbol + "\">" + symbol + "</li>";
              }
            }
            html1 = html1 + "</ul></li>";
          }
        }
      }
    }
  }

  for(var ckey in watchList) {
    if(watchList.hasOwnProperty(ckey)) {
      if(ckey != country) {
        html2 = "<li id=\"SECTION002\">" + ckey + "<ul>";
        for(var mkey in watchList[ckey]) {
          if(watchList[ckey].hasOwnProperty(mkey)) {
            market = mkey;
            html2 = html2 + "<li class=\"LinkedList\" id=\"\">" + market + "<ul>";
            for(var skey in watchList[ckey][mkey]) {
              if(watchList[ckey][mkey].hasOwnProperty(skey)) {
                symbol = skey;
            //for(var symbol in watchList[ckey][mkey]) {
              //symbol = watchList[ckey][mkey][i];
                html2 = html2 + "<li class=\"LinkedList\" id=\"" + symbol + "\">" + symbol + "</li>";
              }
            }
            html2 = html2 + "</ul></li>";
          }
        }
      }
    }
  }

  html = "<div><ul id=\"StocksList\" class=\"LinkedList\" style=\"padding-left:20px\">";
	html = html + html1 + "</ul></li></ul></div><div><h4 style=\"margin-bottom:0px\">" + tr("International Markets", lg) + "</h4><hr>";
	html = html + "<ul id=\"CountryList\" class=\"LinkedList\" style=\"padding-left:20px\">";
	html = html + html2 + "</ul></li></ul></div>";
  return html;
}

function stockData(stockId, data, language)
{
  //currentPermissions = data.substr(0, data.indexOf(':'));
  //data = data.substr(data.indexOf(':') + 1);
// Yahoo
  var ask = data["quoteResponse"]["result"]["0"]["ask"];
  var askSize = data["quoteResponse"]["result"]["0"]["askSize"];
  var averageDailyVolume3Month = data["quoteResponse"]["result"]["0"]["averageDailyVolume3Month"];
  var averageDailyVolume10Day = data["quoteResponse"]["result"]["0"]["averageDailyVolume10Day"];
  var bid = data["quoteResponse"]["result"]["0"]["bid"];
  var bidSize = data["quoteResponse"]["result"]["0"]["bidSize"];
  var exchangeDataDelayedBy = data["quoteResponse"]["result"]["0"]["exchangeDataDelayedBy"];
  var fiftyTwoWeekLow = data["quoteResponse"]["result"]["0"]["fiftyTwoWeekLow"];
  var fiftyTwoWeekHigh = data["quoteResponse"]["result"]["0"]["fiftyTwoWeekHigh"];
  var longName = data["quoteResponse"]["result"]["0"]["longName"];
  var marketCap = data["quoteResponse"]["result"]["0"]["marketCap"];
    // use marketCap.toLocaleString() to insert commas
  var regularMarketDayHigh = data["quoteResponse"]["result"]["0"]["regularMarketDayHigh"];
  var regularMarketDayLow = data["quoteResponse"]["result"]["0"]["regularMarketDayLow"];
  var regularMarketOpen = data["quoteResponse"]["result"]["0"]["regularMarketOpen"];
  var regularMarketPreviousClose = data["quoteResponse"]["result"]["0"]["regularMarketPreviousClose"];
  var regularMarketPrice = data["quoteResponse"]["result"]["0"]["regularMarketPrice"];
  var regularMarketVolume = data["quoteResponse"]["result"]["0"]["regularMarketVolume"];
  var regularMarketChange = data["quoteResponse"]["result"]["0"]["regularMarketChange"];
  var regularMarketChangePercent = data["quoteResponse"]["result"]["0"]["regularMarketChangePercent"];
  var regularMarketTime = data["quoteResponse"]["result"]["0"]["regularMarketTime"];
  var sharesOutstanding = data["quoteResponse"]["result"]["0"]["sharesOutstanding"];
  // use sharesOutstanding.toLocaleString() to insert commas
  var sourceInterval = data["quoteResponse"]["result"]["0"]["sourceInterval"];
  var symbol = data["quoteResponse"]["result"]["0"]["symbol"];

  var tradeable = data["quoteResponse"]["result"]["0"]["tradeable"];


  var marketDateTime = new Date(regularMarketTime * 1000);
  var price = format(regularMarketPrice, 2);
  var change = format(regularMarketChange, 2, "+");
  var changePercent = format(regularMarketChangePercent, 2, "%");
  symbol = "(" + symbol + ")";
  ask = format(ask, 2, "<>", null, regularMarketPreviousClose);
  askSize = format(askSize, 0, "", "grey");
  bid = format(bid, 2, "<>", null, regularMarketPreviousClose);
  bidSize = format(bidSize, 0, "", "grey");
  fiftyTwoWeekLow = format(fiftyTwoWeekLow, 2, "<>", null, regularMarketPreviousClose);
  fiftyTwoWeekHigh = format(fiftyTwoWeekHigh, 2, "<>", null, regularMarketPreviousClose);
  marketCap = format(marketCap, 2);
  marketDayHigh = format(regularMarketDayHigh, 2, "<>", null, regularMarketPreviousClose);
  marketDayLow = format(regularMarketDayLow, 2, "<>", null, regularMarketPreviousClose);
  marketOpen = format(regularMarketOpen, 2, "<>", null, regularMarketPreviousClose);
  marketPreviousClose = format(regularMarketPreviousClose, 2);
  marketTime = formatString(marketDateTime.toLocaleTimeString(), "blue");
  marketDate = formatString(marketDateTime.toDateString(), "blue");
  marketVolume = format(regularMarketVolume, 0);
  dailyVolume10Day = format(averageDailyVolume10Day, 0);
  dailyVolume3Month = format(averageDailyVolume3Month, 0);
  sharesOutstanding = format(sharesOutstanding, 0);

  var html = "<div><p style=\"font-size:18px;display:inline-block;float:left;max-width:360px;\">" + longName.big() + "&nbsp" + symbol.big() + "</p>"
       + "<p style=\"font-size:18px;display:inline-block;float:right;width:260px;\"><span style=\"color:blue;\">" + price.big() + "</span>&nbsp&nbsp&nbsp" + change + "&nbsp&nbsp" + changePercent + "</p></div>" + "<div><hr style=\"background-color:black;height:3px;width:100%;\"></div>"
       + "<div class=\"newsframewrapper\" id=\"newsfeed\"><iframe class=\"newsframe\" id=\"newsframe\" height=\"400\" width=\"2\" src=\"\" style=\"border-width:0\" frameborder=\"0\" ></iframe></div>";
  html = html + "<div class=\"halfsection\" id=\"marketinfo\" style=\"width:320px;float:left;\">";

  html = html + "<p style=\"font-size:14px;width:260px;margin-bottom:0px;display:inline-block;\">Market Time: "	+ marketTime + "</p>";
  html = html + "<p style=\"font-size:14px;width:260px;margin-top:0px;display:inline-block;\">Market Date: "	+ marketDate + "</p>";
  html = html + "<p style=\"font-size:14px;width:300px;\">Market Volume :&ensp;"	+ marketVolume + "</p>";
  html = html + "<p style=\"font-size:14px;width:300px;\">Average Daily Volumes</p>";
  html = html + "<p style=\"font-size:14px;width:300px;\">&emsp;&emsp;&emsp;&emsp;10Day :&ensp;"	+ dailyVolume10Day + "</p>";
  html = html + "<p style=\"font-size:14px;width:300px;\">&emsp;&emsp;&emsp;&ensp;3Month :&ensp;"	+ dailyVolume3Month + "</p>";
  html = html + "<p style=\"font-size:14px;width:300px;\">Shares :&ensp;"	+ sharesOutstanding + "</p>";
  html = html + "<p style=\"font-size:14px;width:300px;\">Market Cap £: "	+ marketCap + "</p>";


  html = html + "</div>";
  html = html + "<div class=\"halfsection\" id=\"marketprice\" style=\"width:260px;float:right;\">";

  html = html + "<p style=\"font-size:14px;width:130px;display:inline-block;\">ask:&ensp;"	+ ask + "<br>vol&nbsp;:&ensp;"	+ askSize + "</p>";
  html = html + "<p style=\"font-size:14px;width:130px;display:inline-block;\">bid:&ensp;" + bid + "<br>vol:&ensp;" + bidSize + "</p>";
  html = html + "<p style=\"font-size:14px;width:260px;display:inline-block;margin-top:3px;\">Previous Close :&ensp;" + marketPreviousClose + "<br>Market Open :&emsp;&ensp;&nbsp" + marketOpen + "</p>";
  //html = html + "<p style=\"font-size:14px;width:260px;display:inline-block;\">Open: " + marketOpen + "</p>";

  html = html + "<p style=\"font-size:14px;width:130px;display:inline-block;margin-top:3px;\">High: "	+ marketDayHigh + "</p>";
  html = html + "<p style=\"font-size:14px;width:130px;display:inline-block;margin-top:3px;\">Low: " + marketDayLow + "</p>";
  html = html + "<hr style=\"background-color:blue;height:1px;width:100%;\">";
  html = html + "<p style=\"font-size:14px;width:260px;display:inline-block;margin-top:3px;margin-bottom:3px;\">52 week Market Data" + "</p>";
  html = html + "<p style=\"font-size:14px;width:130px;display:inline-block;\">High: "	+ fiftyTwoWeekHigh + "</p>";
  html = html + "<p style=\"font-size:14px;width:130px;display:inline-block;\">Low: " + fiftyTwoWeekLow + "</p>";
  html = html + "</div>";

  return html;
}

function watchListData(market, data, language) {
  var longName = "";
  var symbol = "";

  var regularMarketDayHigh = "";
  var regularMarketDayLow = "";
  var regularMarketOpen = "";
  var regularMarketPreviousClose = "";
  var regularMarketPrice = "";
  var regularMarketVolume = "";
  var regularMarketChange = "";
  var regularMarketChangePercent = "";
  var regularMarketTime = "";

  var price = "";
  var change = "";
  var changePercent = "";

  html = "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:100%;margin:0px;margin-bottom:5px;padding:0px\">";
  html = html + "<thead style=\"padding:0px; margin: 0px;\"><tr bgcolor=\"#f0e0f0\">";
  html = html + "<th scope=\"col\" style=\"padding:0px; margin: 0px; font-family: Arial,sans-serif; font-size: 14px; text-align: left;line-height:20px\">symbol</th>";
  html = html + "<th scope=\"col\" style=\"padding:0px; margin: 0px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px\">stock</th>";
  html = html + "<th scope=\"col\" style=\"padding:0px; margin: 0px; font-family: Arial,sans-serif; font-size: 14px; text-align: right;line-height:20px\">previous</th>";
  html = html + "<th scope=\"col\" style=\"padding:0px; margin: 0px; font-family: Arial,sans-serif; font-size: 14px; text-align: right;line-height:20px\">price</th>";
  html = html + "<th scope=\"col\" style=\"padding:0px; margin: 0px; font-family: Arial,sans-serif; font-size: 14px; text-align: right;line-height:20px\">change</th>";
  html = html + "<th scope=\"col\" style=\"padding:0px; margin: 0px; font-family: Arial,sans-serif; font-size: 14px; text-align: right;line-height:20px\">change %</th>";
  html = html + "</tr></thead>";
  html = html + "<tbody>";
  var tint = "#f9f5f5";
  for(var key in data["quoteResponse"]["result"]) {
    if(data["quoteResponse"]["result"].hasOwnProperty(key)) {
      longName = data["quoteResponse"]["result"][key]["longName"];
      symbol = data["quoteResponse"]["result"][key]["symbol"];
      symbol = "(" + symbol + ")";

      regularMarketDayHigh = data["quoteResponse"]["result"][key]["regularMarketDayHigh"];
      regularMarketDayLow = data["quoteResponse"]["result"][key]["regularMarketDayLow"];
      regularMarketOpen = data["quoteResponse"]["result"][key]["regularMarketOpen"];
      regularMarketPreviousClose = data["quoteResponse"]["result"][key]["regularMarketPreviousClose"];
      regularMarketPrice = data["quoteResponse"]["result"][key]["regularMarketPrice"];
      regularMarketVolume = data["quoteResponse"]["result"][key]["regularMarketVolume"];
      regularMarketChange = data["quoteResponse"]["result"][key]["regularMarketChange"];
      regularMarketChangePercent = data["quoteResponse"]["result"][key]["regularMarketChangePercent"];
      regularMarketTime = data["quoteResponse"]["result"][key]["regularMarketTime"];

      previousClose = format(regularMarketPreviousClose, 2, "", "black");
      price = format(regularMarketPrice, 2, "<>", null, regularMarketPreviousClose);
      change = format(regularMarketChange, 2, "+");
      changePercent = format(regularMarketChangePercent, 2, "%");
      marketTime = new Date(regularMarketTime);

      html = html + "<tr bgcolor=\"" + tint + "\">";
      html = html + "<td valign=\"top\" style=\"padding:0px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px;\">" + symbol + "</td>";
      html = html + "<td valign=\"top\" style=\"padding:0px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px;\">" + longName + "</td>";
      html = html + "<td valign=\"top\" style=\"padding:0px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px;text-align: right;\">" + previousClose + "</td>";
      html = html + "<td valign=\"top\" style=\"padding:0px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px;text-align: right;\">" + price + "</td>";
      html = html + "<td valign=\"top\" style=\"padding:0px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px;text-align: right;\">" + change + "</td>";
      html = html + "<td valign=\"top\" style=\"padding:0px; font-family: Arial,sans-serif; font-size: 14px; line-height:20px;text-align: right;\">" + changePercent + "</td>";
      html = html + "</tr>";
      if(tint == "#f9f5f5") tint = "#f0e5e5";
      else tint = "#f9f5f5";
    }
  }
  html = html + "</tbody>";
  html = html + "</table>";
  return html;
}
