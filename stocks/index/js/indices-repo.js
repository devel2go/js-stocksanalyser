const AER = "Aerospace and Defence";
const ALT = "Alternative Energy";
const AUT = "Automobiles and Parts";
const BAN = "Banks";
const BEV = "Beverages";
const CHE = "Chemicals";
const CON = "Construction and Materials";
const ELE = "Electricity";
const ELT = "Electronic and Electrical Equipment";
const EQU = "Equity Investment Instruments";
const OIL = "Oil and Gas Procuders";

function indexList(stockExchange)
{
  var ilist = [];
  switch(stockExchange) {
    case "LSE":
      ilist = ["FTSE-100", "FTSE-250", "FTSE-350", "FTSE-AllShare", "FTSE-SmallCap", "AIM-100", "AIM-AllShare"];
      break;
  }
  return ilist;
}

function sectorList(stockExchange, marketIndex)
{
  var slist = [];
  if(stockExchange == "LSE") {
      slist = [AER, ALT, AUT, BAN, BEV, CHE, CON, ELE, ELT, EQU, OIL];
  return slist;}
}

function tickerList(stockExchange, marketIndex, sector)
{
  var tlist = [];
  if(stockExchange == "LSE") {
    switch(marketIndex) {
      case "FTSE-100":
        if(sector) {
          switch(sector) {
            case "OIL_AND_GAS":
              tlist = ["BP..L", "RDSA.L", "RDSB.L"];
              break;
          }
        }
        break;
      case "FTSE-250":
        if(sector) {
          switch(sector) {
            case "OIL_AND_GAS":
              tlist = ["CNE.L", "ENOG.L", "PMO.L", "TLW.L"];
              break;
          }
        }
        break;
      case "FTSE-350":
        if(sector) {
          switch(sector) {
            case "OIL_AND_GAS":
              tlist = ["BP..L", "CNE.L", "ENOG.L", "PMO.L", "RDSA.L", "RDSB.L", "TLW.L"];
              break;
          }
        }
        break;
      case "FTSE-AllShare":
        if(sector) {
          switch(sector) {
            case "OIL_AND_GAS":
              tlist = ["BP..L", "CNE.L", "ENOG.L", "ENQ.L", "NOG.L", "OPHR.L", "PMO.L", "RDSA.L", "RDSB.L", "SIA.L","TLW.L"];
              break;
          }
        }
        break;
      case "FTSE-SmallCap":
        if(sector) {
          switch(sector) {
            case "OIL_AND_GAS":
              break;
            default:
              tlist = ["FOUR.L", "AAIF.L", "AAS.L", "ADIG.L", "ABD.L", "ANII.L", "ASLI.L", "ASIT.L"];
          }
        }
        break;
      case "AIM-100":
        if(sector) {
          switch(sector) {
            case AUT:
              tlist = ["AUTG.L", "SCE.L", "TRT.L"];
              break;
            case BAN:
              tlist = [];
              break;
            case CON:
              tlist = ["AXS.L", "ASH.L", "AUK.L", "BILN.L", "EPWN.L", "FOX.L", "INSP.L", "JHD.L", "MBH.L", "MOGP.L", "NEXS.L", "SRC.L", "VANL.L", "BREE.L"];
              break;
            case OIL:
              tlist = ["DGOC.L", "ELA.L", "FPM.L", "HUR.L", "SAVP.L", "SOU.L"];
              break;
            default:
              tlist = ["88E.L", "AEG.L", "AFC.L", "RKH.L"];
          }
        }
        break;
      default:
        tlist = [];
    }
  }
  return tlist;
}
