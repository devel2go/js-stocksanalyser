
// Add this to the onload event of the BODY element
function setupIndex(list, currentId)
{
	setupTree(list);
	activateTree(list, currentId);
}
function updateIndex(list, currentId)
{
	var cSubBranches;
	var items;

	console.log("currentId: " + currentId); 
	// Expand ClubList item currentId
	
	cSubBranches = list.getElementsByTagName("ul");
	var ul_total = cSubBranches.length;
	//console.log("....list.getElementsByTagName(ul) = ");
	//console.log("Found.... " + ul_total.toString() + " SubBranches ul");

	cSubBranchTitles = list.getElementsByTagName("li");
	var li_total = cSubBranchTitles.length;
	//console.log("....list.getElementsByTagName(li) = ");
	//console.log("Found.... " + li_total.toString() + " SubBranches li");
	var li_count = 0;
	var li_head;
	/*cSubBranchTitles[li_count].style.color = "grey";
	$(cSubBranchTitles[li_count]).hover(function(){
		this.style.color = "black";
	}, function(){
		this.style.color = "grey";
	});*/
	if(li_total > 0)
	{
		if (ul_total > 0)
		{
			for (var ul_count = 0; ul_count < ul_total; ul_count++)
			{
				//console.log("ul_count = " + ul_count.toString());
				//console.log("li_count = " + li_count.toString());
				if(cSubBranchTitles[li_count].id.substr(0,7) == "SECTION")
				{
					cSubBranchTitles[li_count].style.color = "#8080ff";		/* light blue */
					//console.log("cSubBranchTitles[" + li_count.toString() + "].style.color='magenta'");
					$(cSubBranchTitles[li_count]).hover(function(){
						this.style.color = "#0000ff";		/* blue */
					}, function(){
						this.style.color = "#8080ff";		/* light blue */
					});
					li_head = li_count;
				}
				cSubSubBranches = cSubBranches[ul_count].getElementsByTagName("ul");
				var ulul_total = cSubSubBranches.length;
				//console.log("....cSubBranche[" + ul_count.toString() + "].getElementsByTagName(ul) = ");
				//console.log("Found.... " + ulul_total.toString() + " SubSubBranches ul");
				if(ulul_total < 1)
				{
					// No sub branches just list items
					items = cSubBranches[ul_count].getElementsByTagName("li");
					var ulli_total = items.length;
					//console.log("....cSubBranche[" + ul_count.toString() + "].getElementsByTagName(li) = ");
					//console.log("Found.... " + ulli_total.toString() + " itemSubBranches li");
					if(items.length > 0)
					{
						for(var ulli_count = 0; ulli_count < ulli_total; ulli_count++)
						{
							//console.log("ulli_count = " + ulli_count.toString());
							if(items[ulli_count].id == currentId)
							{
								items[ulli_count].style.color = "#0000ff";		/* blue */
								$(items[ulli_count]).hover(function(){
									this.style.color = "#0000ff";		/* blue */
								}, function(){
									this.style.color = "#0000ff";		/* blue */
								});
								cSubBranchTitles[li_count].style.color = "#0000ff";		/* blue */
								$(cSubBranchTitles[li_count]).hover(function(){
									this.style.color = "#0000ff";		/* blue */
								}, function(){
									this.style.color = "#0000ff";		/* blue */
								});
							}
							else
							{
								items[ulli_count].style.color = "#8080ff";		/* light blue */
								$(items[ulli_count]).hover(function(){
									this.style.color = "#0000ff";		/* blue */
								}, function(){
									this.style.color = "#8080ff";		/* light blue */
								});
								
							}
						}
						li_count = li_count + ulli_total + 1;
					}
				}
				else
				{
					// found sub branches
					li_count = li_count + 1;
					for(var ulul_count = 0; ulul_count < ulul_total; ulul_count++)
					{
						cSubSubSubBranches = cSubSubBranches[ulul_count].getElementsByTagName("ul");
						var ululul_total = cSubSubSubBranches.length;
						//console.log("....cSubSubBranche[" + ul_count.toString() + "].getElementsByTagName(ul) = ");
						//console.log("Found.... " + ululul_total.toString() + " SubSubSubBranches ul");
						if(ululul_total < 1)
						{
							// No sub branches just list items
							items = cSubSubBranches[ulul_count].getElementsByTagName("li");
							var ululli_total = items.length;
							//console.log("....cSubSubBranche[" + ulul_count.toString() + "].getElementsByTagName(li) = ");
							//console.log("Found.... " + ululli_total.toString() + " itemSubSubBranches li");
							if(items.length > 0)
							{
								cSubBranchTitles[li_count].style.color = "#8080ff";		/* light blue */
								$(cSubBranchTitles[li_count]).hover(function(){
									this.style.color = "#0000ff";		/* blue */
								}, function(){
									this.style.color = "#8080ff";		/* light blue */
								});
								for(var ululli_count = 0; ululli_count < ululli_total; ululli_count++)
								{
									//console.log("ululli_count = " + ululli_count.toString());
									if(items[ululli_count].id == currentId)
									{
										items[ululli_count].style.color = "#0000ff";		/* blue */
										$(items[ululli_count]).hover(function(){
											this.style.color = "#0000ff";		/* blue */
										}, function(){
											this.style.color = "#0000ff";		/* blue */
										});
										cSubBranchTitles[li_count].style.color = "#0000ff";		/* blue */
										$(cSubBranchTitles[li_count]).hover(function(){
											this.style.color = "#0000ff";		/* blue */
										}, function(){
											this.style.color = "#0000ff";		/* blue */
										});
										cSubBranchTitles[li_head].style.color = "#0000ff";		/* blue */
										$(cSubBranchTitles[li_head]).hover(function(){
											this.style.color = "#0000ff";		/* blue */
										}, function(){
											this.style.color = "#0000ff";		/* blue */
										});
									}
									else
									{
										items[ululli_count].style.color = "#8080ff";		/* light blue */
										$(items[ululli_count]).hover(function(){
											this.style.color = "#0000ff";		/* blue */
										}, function(){
											this.style.color = "#8080ff";		/* light blue */
										});
										
									}
								}
								li_count = li_count + ululli_total + 1;
							}
							else
							{
							}
						}
					}
					ul_count = ul_count + ulul_total;
				}
			}
		}
	}
}

function activateTree(olist, itemId)
{
	 var cSubBranches;
	 var items;
	 var e;
	console.log("itemId" + itemId); 
	// Expand ClubList item currentId
	cSubBranches = olist.getElementsByTagName("ul");
	var d = cSubBranches.length;
	//console.log("Found.... " + d.toString() + " SubBranches");
	if (cSubBranches.length > 0)
	{
		for (var i=1; i < cSubBranches.length; i++)
		{
			items = cSubBranches[i].getElementsByTagName("li");
			e = items.length;
			//console.log("Found.... " + e.toString() + " list items");
			if(items.length > 0)
			{
				for(l = 0; l < items.length; l++)
				{
					//console.log("items id = " + items[l].id);
					if(items[l].id == itemId)
					{
						//console.log("Found.... item[" + l.toString() + "].id = " + itemId);
						//items[l].style.color = "red";
						cSubBranches[i].style.display = "block";
					}
				}
			}
		}
	}
}
// This function traverses the list and add links 
// to nested list items
function setupTree(oList) 
{
	var cSubBranches;
	// Collapse the tree
	for (var i=0; i < oList.getElementsByTagName("ul").length; i++)
	{
		oList.getElementsByTagName("ul")[i].style.display="none";            
	}

	// Add the click-event handler to the list items
	if (oList.addEventListener) {
		oList.addEventListener("click", toggleBranch, false);
	} else if (oList.attachEvent) { // For IE
		oList.attachEvent("onclick", toggleBranch);
	}
	// Make the nested items look like links
	addLinksToBranches(oList);
	// Expand ClubList
	cSubBranches = oList.getElementsByTagName("ul");
	if (cSubBranches.length > 0) {
		if (cSubBranches[0].style.display == "block") {
			cSubBranches[0].style.display = "none";
		}
		else {
			cSubBranches[0].style.display = "block";
		}
	}
}
 
// This is the click-event handler
function toggleBranch(event) {
  var oBranch, cSubBranches;
  if (event.target) {
	oBranch = event.target;
  } else if (event.srcElement) { // For IE
	oBranch = event.srcElement;
  }
  cSubBranches = oBranch.getElementsByTagName("ul");
  if (cSubBranches.length > 0) {
	if (cSubBranches[0].style.display == "block") {
	  cSubBranches[0].style.display = "none";
	} else {
	  cSubBranches[0].style.display = "block";
	}
  }
  else
	linkClicked(oBranch.id);
}
 
    // This function makes nested list items look like links
function addLinksToBranches(oList) {
	var cBranches = oList.getElementsByTagName("li");
	var i, n, cSubBranches;
	if (cBranches.length > 0) {
		cBranches[0].className = "HandCursorStyle";
		cBranches[0].style.color = "black";
		for (i=1, n = cBranches.length; i < n; i++) {
			cSubBranches = cBranches[i].getElementsByTagName("ul");
			if (cSubBranches.length > 0) {
				addLinksToBranches(cSubBranches[0]);
				cBranches[i].className = "HandCursorStyle";
				cSubBranches[0].style.cursor = "HandCursorStyle";
			}
		}
	}
}


	