// html-format.js

//var moment = require("moment-timezone");
var dateFormats = ['YYYY-MM-DD', 'MM/DD/YYYY'];

function toDate(value, valueForError) {
  try {
    var date = moment.tz(value, dateFormats, 'America/New_York').toDate();
    if (date.getFullYear() < 1400) { return null; }
    return date;
  } catch (err) {
    if (_.isUndefined(valueForError)) {
      return null;
    } else {
      return valueForError;
    }
  }
}
function formatString(str, color)
{
  return  "<span style=\"color:" + color + ";\">" + str + "</span>";
}
// Forces signing on a number, returned as a string
function format(num, dec, type, color, comp)
{
  if(!comp) comp = 0;
  if(num > comp){
    fnum = num.toLocaleString(undefined, {minimumFractionDigits: dec, maximumFractionDigits: dec});
		//fnum = num.toFixed(dec);
    if(!color) color = "blue";
		if(type == "%"){
        	return  "<span style=\"color:green;\">" + "(+" + fnum + "%)</span>";
    }else if(type == "+"){
          return  "<span style=\"color:green;\">" + "+" + fnum + "</span>";
    }else if(type == "<>"){
          return  "<span style=\"color:green;\">" + fnum + "</span>";
		}else{
          return  "<span style=\"color:" + color + ";\">" + fnum + "</span>";
		}
  }else if(num < comp){
    fnum = num.toLocaleString(undefined, {minimumFractionDigits: dec, maximumFractionDigits: dec});
	  if(type == "%"){
      return  "<span style=\"color:red;\">" + "(" + fnum + "%)</span>";
    }else if(type == "+"){
          return  "<span style=\"color:red;\">" + fnum + "</span>";
    }else if(type == "<>"){
      return  "<span style=\"color:red;\">" + fnum + "</span>";
    }else{
      return  "<span style=\"color:red;\">" + fnum + "</span>";
		}
	}else{
    fnum = num.toLocaleString(undefined, {minimumFractionDigits: dec, maximumFractionDigits: dec});
		if(type == "%"){
        	return  "<span style=\"color:blue;\">("+ fnum + "%)</span>";
    }else if(type == "+"){
          return  "<span style=\"color:blue;\">"+ fnum + "</span>";
    }else if(type == "<>"){
          return  "<span style=\"color:blue;\">"+ fnum + "</span>";
		}else{
			return  "<span style=\"color:" + color + ";\">"+ fnum + "</span>";
		}
    }
}
