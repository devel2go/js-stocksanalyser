/* stock-analyser/fileserver/server.js */

var crypto = require("crypto");
var fs = require("fs");
var https = require("https");
var path = require("path");
var url = require("url");
var qs = require("querystring");

function start(router, handle)
{
	function onRequest(request, response)
	{
		var url_parts = url.parse(request.url, true);
		console.log("url_parts = " + JSON.stringify(url_parts));
		var pathname = url_parts.pathname;
		console.log("url_parts.pathname = " + pathname);
		//console.log("url_parts.username = " + url_parts.username);
		//var body = "";
		request.on("data", function(data)
		{
			body += data;
			//console.log("got data:" + data);
		});
		request.on("end", function()
		{
			//console.log("end data:" + data);
			console.log("GET request for " + pathname);
			if(request.method === "GET")
			{
				var params = url.parse(request.url,true).query;
				//console.log("params =" + JSON.stringify(params));
				if(pathname === "/stocks/getdata")
					router.getdata(handle, pathname, params, request, response);
				else
					router.get(handle, pathname, request, response);
			}
			/*else if(request.method === "POST")
			{
				console.log("POST request for " + pathname);
				var params = qs.parse(body);
				console.log("params = " + JSON.stringify(params));
				router.post(handle, pathname, params, request, response);
			}*/
		});
	}
	const args = process.argv;
	var dirPath = path.dirname(args[1]);
	var portHTTPS = args[2];
	console.log("Server dirPath: " + dirPath);
	if(!portHTTPS) portHTTPS = "8000"
	const options = {
		key: fs.readFileSync(dirPath + '/keys/stocks-server.key'),
		cert: fs.readFileSync(dirPath + '/keys/stocks-server.crt'),
	}
	var server = https.createServer(options);
	server.addListener("request", onRequest);
	server.listen(portHTTPS);
	console.log("NodeJS Stocks Server has started on port: " + portHTTPS);
}

exports.start = start;
