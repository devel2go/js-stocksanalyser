/* nodejs/test/router.js */

function get(handle, pathname, request, response)
{
	console.log("About to route a GET request for " + pathname);
	if (typeof handle[pathname] === 'function')
	{
		if(pathname == "/login")
			handle[pathname](request, response);
		else if(pathname == "/logout")
			handle[pathname](request, response);
		else
			handle[pathname](pathname, response);
	}
	else
	{
		console.log("No request handler found for " + pathname);
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.write("404 File Not found");
		response.end();
	}
}
function getdata(handle, pathname, params, request, response)
{
	console.log("About to route a GETDATA request for " + pathname);
	if (typeof handle[pathname] === 'function')
	{
		handle[pathname](params, request, response);
	}
	else
	{
		console.log("No request handler found for " + pathname);
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.write("404 File Not found");
		response.end();
	}
}

exports.get = get;
exports.getdata = getdata;
