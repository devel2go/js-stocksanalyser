/* nodejs/fileserver/index.js */

var server = require("./server");
var router = require("./router");
var requestHandler = require("./request_handler");


// requests are directed to request handler

var handle = {};
//handle["/indexlist"] = requestHandler.indexList;
handle["/stocks"] = requestHandler.index;
handle["/stocks/getdata"] = requestHandler.getData;
handle["/stocks/index"] = requestHandler.index;
handle["/stocks/index/offline_stocks.html"] = requestHandler.index;
handle["/stocks/index/css/dialog.css"] = requestHandler.filecss;
handle["/stocks/index/css/flags.css"] = requestHandler.filecss;
handle["/stocks/index/css/menu.css"] = requestHandler.filecss;
handle["/stocks/index/css/popup.css"] = requestHandler.filecss;
handle["/stocks/index/css/styles.css"] = requestHandler.filecss;
handle["/stocks/index/css/tabs.css"] = requestHandler.filecss;
handle["/stocks/index/css/treeview.css"] = requestHandler.filecss;
handle["/stocks/index/images/flags/flags.png"] = requestHandler.filepng;
handle["/stocks/index/images/headers/stocksanalyser.jpg"] = requestHandler.filejpg;
handle["/stocks/index/js/cookie.js"] = requestHandler.filejs;
handle["/stocks/index/js/country.js"] = requestHandler.filejs;
handle["/stocks/index/js/dialog.js"] = requestHandler.filejs;
handle["/stocks/index/js/database.js"] = requestHandler.filejs;
handle["/stocks/index/js/html-format.js"] = requestHandler.filejs;
handle["/stocks/index/js/html-manager.js"] = requestHandler.filejs;
handle["/stocks/index/js/jquery-2.1.1.min.js"] = requestHandler.filejs;
handle["/stocks/index/js/script.js"] = requestHandler.filejs;
handle["/stocks/index/js/table.js"] = requestHandler.filejs;
handle["/stocks/index/js/tabs.js"] = requestHandler.filejs;
handle["/stocks/index/js/translator.js"] = requestHandler.filejs;
handle["/stocks/index/js/treeview.js"] = requestHandler.filejs;
handle["/stocks/index/images/adverts/ads1.jpg"] = requestHandler.filejpg;
handle["/stocks/index/images/adverts/ads2.jpg"] = requestHandler.filejpg;
handle["/stocks/index/images/adverts/ads3.jpg"] = requestHandler.filejpg;
server.start(router, handle);
